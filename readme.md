# Des données sur l'univers tout entier

À travers cet exercice vous allez devoir récupérer diverses données fournies par la NASA afin de répondre aux besoins énoncés.

L'ensemble de cet exercice doit être réalisé en utilisant les langages suivants : HTML, CSS, JS. Il est également possible de le faire en PHP si vous le souhaitez.

## Indications

Vous devez utiliser les APIs de la NASA : *https://api.nasa.gov/*.
Afin d'obtenir une clé de leur API vous devez simplement renseigner votre nom, prénom et adresse mail. Cette clé est nécessaire pour accéder à leurs APIs.

Pour la partie 1 vous devrez utiliser l'API <ins>Mars Rover Photos</ins>.
Pour la partie 2 vous devrez utiliser l'API <ins>NASA Image and Video Library</ins>.

Pour l'aspect visuel de la page aucun thème n'est imposé, laissez parler votre imagination. Si le thème vous intéresse vous pouvez également aller plus loin dans l'exercice en implémentant d'autres fonctionnalités.

# 1. Des photos de la planète rouge...

Pour commencer vous devez faire appel à l'API de la NASA afin de récupérer, aléatoirement, une photo prise par le rover Curiosity lors de son voyage sur Mars. Dans un souci d'esthétique, vous devrez récupérer uniquement les photos de la caméra de navigation de Curiosity.

Pour ce faire, l'utilisateur doit pouvoir choisir la date de la photo qui va être récupérée à l'aide d'un input. Une fois la photo aléatoire récupérée, vous devrez l'afficher dans la page. Si jamais aucune photo n'est trouvée pour la date, pensez à n'afficher qu'aucune photo n'a été trouvée.

# 2. Des photos d'à peu près tout...

Les photos de Curiosity sont intéressantes mais vous devez maintenant récupérer toutes sortes de photos selon la recherche de l'utilisateur. Comme pour la récupération des photos de Curiosity, vous en afficherez une aléatoire parmi les résultats de la recherche et vous l'afficherez sur la page.

L'utilisateur doit pouvoir entrer sa recherche dans un input, il utilisera un bouton pour lancer la recherche correspondant à ce qu'il a indiqué dans l'input.